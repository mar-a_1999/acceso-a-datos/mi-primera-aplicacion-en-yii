<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Equipo".
 *
 * @property string $nomequipo
 * @property string|null $director
 */
class Equipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Equipo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nomequipo'], 'required'],
            [['nomequipo'], 'string', 'max' => 25],
            [['director'], 'string', 'max' => 30],
            [['nomequipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nomequipo' => 'Nomequipo',
            'director' => 'Director',
        ];
    }
}
