<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Lleva".
 *
 * @property int $dorsal
 * @property int $numetapa
 * @property string $código
 */
class Lleva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Lleva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dorsal', 'numetapa', 'código'], 'required'],
            [['dorsal', 'numetapa'], 'integer'],
            [['código'], 'string', 'max' => 3],
            [['numetapa', 'código'], 'unique', 'targetAttribute' => ['numetapa', 'código']],
            [['numetapa'], 'exist', 'skipOnError' => true, 'targetClass' => Etapa::className(), 'targetAttribute' => ['numetapa' => 'numetapa']],
            [['código'], 'exist', 'skipOnError' => true, 'targetClass' => Maillot::className(), 'targetAttribute' => ['código' => 'código']],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::className(), 'targetAttribute' => ['dorsal' => 'dorsal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dorsal' => 'Dorsal',
            'numetapa' => 'Numetapa',
            'código' => 'Código',
        ];
    }
}
